﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitErrorQueueParser
{
    public static class CsvExtension
    {
        public static string FormatForCsv(this string value)
        {
            return value.Replace("\"", "'");
        }
    }
}
