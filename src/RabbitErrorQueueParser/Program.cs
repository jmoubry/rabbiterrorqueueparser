﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace RabbitErrorQueueParser
{
    class Program
    {
        // NETSUITE
        //private static string[] errorMessagesToExcludeFirst35 = {
        //    "DUP_ENTITY: This entity already exi",
        //    "USER_ERROR: You must enter at least one line item for this transaction.".Substring(0, 35),
        //    "INVALID_KEY_OR_REF: The specified key is invalid.".Substring(0, 35),
        //    "RCRD_HAS_BEEN_CHANGED: Record has been changed".Substring(0, 35),
        //    "Sales order could not be found with External ID ".Substring(0, 35),
        //    "SalesOrder could not be found with external ID ".Substring(0, 35),
        //    "The operation has timed out"
        //};

        // ORDER LIFECYCLE
        private static string[] errorMessagesToExcludeFirst35 = {
            //"An error occurred while attempting to translate SKUs. A SKU mapping may be missing.".Substring(0, 35),
            //"The operation has timed out"
        };

        /// <summary>
        /// 0. Clear out the rows in error_log.csv if you don't want to append there.
        /// 1. Get messages from error queue without requeueing.
        /// 2. Hit F12 on the browser and copy the HTML into the input.html file.
        /// 3. Run this program.
        /// 4. Import the csv results into the google sheet: https://docs.google.com/spreadsheets/d/1Yi9VxNEpmlNBkcWKAYYUh2KWnLUD5knumCGWhL4ejQk/edit#gid=1043096621
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var doc = new HtmlDocument();
            doc.Load("T:\\BuildASign\\RabbitErrorQueueParser\\src\\RabbitErrorQueueParser\\input.html");

            var messages = doc.DocumentNode.SelectNodes("//div[@id='msg-wrapper']/div/table[@class='facts']");

            var csvRowForMessage = new List<string>();

            foreach (var message in messages)
            {
                var date = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=5]/td").First().InnerText;
                var errorMessage = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=3]/td").First().InnerText;

                if (errorMessagesToExcludeFirst35.Contains(errorMessage.Substring(0, Math.Min(35, errorMessage.Length))))
                    continue;

                var stackTrace = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=4]/td/abbr").First().InnerText;
                
                var payload = message.SelectNodes("./tbody//pre[@class='msg-payload']").First().InnerText;
                var messageType = Regex.Match(payload, "urn:message:(.*)\"").Groups[1];

                csvRowForMessage.Add($"{date},{messageType},\"{errorMessage.FormatForCsv()}\",\"{stackTrace.FormatForCsv()}\",\"{payload.FormatForCsv()}\"");
            }


            using (var sw = File.AppendText("T:\\BuildASign\\RabbitErrorQueueParser\\src\\RabbitErrorQueueParser\\error_log.csv"))
            {
                sw.WriteLine(string.Join(Environment.NewLine, csvRowForMessage));
            }
        }
    }
}
