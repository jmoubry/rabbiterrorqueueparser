# Hacky Rabbit Error Queue Parser

**Note the input and output file paths are hard-coded to T:\BuildASign\RabbitErrorQueueParser so you'll need to change those if you checked out this repo in a different folder**

## Instructions

1. Use the RabbitMQ UI to get a few thousand messages (you can select the "Ack message requeue false" mode so that the messages you grab will be removed from the queue)
2. Use the browser's developer tools to view the HTML and copy the entire page's HTML and save it off into an input.html file on the rabbit server.
3. Copy the input.html file from the rabbit server to your machine in T:\BuildASign\RabbitErrorQueueParser\src\RabbitErrorQueueParser
4. Make sure T:\BuildASign\RabbitErrorQueueParser\src\RabbitErrorQueueParser\error_log.csv has a line with column header names followed by a blank line and nothing else
5. Run the console app
6. The output file should be populated: T:\BuildASign\RabbitErrorQueueParser\src\RabbitErrorQueueParser\error_log.csv

## Excluding Certain Error Messages

The static variable, errorMessageToExcludeFirst35, can be populated with a list of error messages to ignore (it's only the first 35 characters of the message).


